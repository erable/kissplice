# KisSplice

Main page: http://kissplice.prabi.fr/

[User guide](https://gitlab.inria.fr/erable/kissplice/-/jobs/artifacts/master/file/prefix/share/doc/kissplice/user_guide.pdf?job=with_bundled_bcalm_and_guide) from latest build.

# How to install
Install options by decreasing order of convenience

## Debian/Ubuntu packages
Kissplice is available as a system package in Debian/Ubuntu
```bash
apt install kissplice
kissplice --help
```

## Conda
Kissplice is available in conda in the [bioconda channel](https://bioconda.github.io/).
Bioconda creates packages for Linux (x86_64 and arm64) and MacOS (x86_64 = intel macs before M1).
```bash
conda install -c conda-forge -c bioconda kissplice
```

## Homebrew (MacOS)
Kissplice is available as a brew formula in [brewsci/bio](https://github.com/brewsci/homebrew-bio) in MacOS.
This is the only packaged option for ARM MacOS (M1 and later).
```bash
brew install brewsci/bio/kissplice
```

## Docker
You can find the latest version of KisSplice, KisSplice2RefGenome and KissDE [on Docker Hub](https://hub.docker.com/repository/docker/dwishsan/kissplice-pipeline).

We also propose a [stand-alone Docker image for the KissDE Shiny App](https://hub.docker.com/repository/docker/dwishsan/kde-shiny) for KisSplice results exploration.

## Binary package (Debian/Ubuntu)
A precompiled standalone binary package for ubuntu is available in the [release page](https://gitlab.inria.fr/erable/kissplice/-/releases).
```bash
tar xf kissplice-binary-ubuntu-<version>.tar.gz
kissplice-binary-ubuntu-<version>/bin/kissplice --help
```

## Build from source
Required dependencies:
- cmake >= 3.9
- C/C++14 compiler toolchain (binutils, gcc or clang)
- python3 to run kissplice

Optional dependencies:
- [bcalm](https://github.com/GATB/bcalm) >= v2.2.2. A locally compiled version of bcalm can instead be used by passing `-DUSE_BUNDLED_BCALM=ON` to cmake for convenience, but requires git.
- latex toolchain : only if you request to build the user guide by passing `-DUSER_GUIDE=ON` to cmake

The following commands assume you are a user that is not familiar with cmake, and wants a local install from source with only the required dependencies.
If you are a developper or a maintainer of a package, see the [detailed cmake documentation](readme.cmake.md).

Download a *source code* archive from the latest [release](https://gitlab.inria.fr/erable/kissplice/-/releases) and uncompress it.
You can also clone the git repository, but the latest code may be broken.
```bash
cd kissplice/ # directory where the release tar.gz was uncompressed, or where the git was cloned

# Replace install_directory by the path where you want your install to be.
# If you have latex installed you can add -DUSER_GUIDE=ON to generate the user guide at install_directory/doc/user_guide.pdf
cmake -S . -B build/ -DCMAKE_BUILD_TYPE=Release -DUSE_BUNDLED_BCALM=ON -DCMAKE_INSTALL_PREFIX=install_directory

cmake --build build/ # add -jN to use up to N cpu cores in parallel
cmake --install build/

# Example of use
install_directory/bin/kissplice -r sample_example/mock1.fq -r sample_example/mock2.fq -r sample_example/virus1.fq -r sample_example/virus2.fq
```
