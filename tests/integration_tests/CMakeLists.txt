# ============================================================================
# Basic test: Does kissplice run ?
# ============================================================================
add_test(KisspliceBasicTest "${BUILD_BINDIR}/kissplice" -h)

# ============================================================================
# Tests on: /data/HBM75brain_100000.fasta 
#      +   /data/HBM75liver_100000.fasta
# Comparing the graph and the count file
# ============================================================================
add_test(KisspliceDBGTest "${CMAKE_CURRENT_SOURCE_DIR}/kisspliceDBGTest.py" "${BUILD_BINDIR}")

# ============================================================================
# Tests on: /data/HBM75brain_100000.fasta 
#      +   /data/HBM75liver_100000.fasta
# ============================================================================
add_test(KisspliceTwoSequencesTest "${CMAKE_CURRENT_SOURCE_DIR}/kisspliceTwoSequencesTest.py" "${BUILD_BINDIR}")
set_tests_properties(KisspliceTwoSequencesTest PROPERTIES PASS_REGULAR_EXPRESSION "test SUCCESSFUL")

# ============================================================================
# Tests on: //data/graph_HBM75brain_100000_HBM75liver_100000_k25 + 
#              /data/HBM75brain_100000.fasta
# ============================================================================

add_test(KisspliceGraphAndSequenceTest "${CMAKE_CURRENT_SOURCE_DIR}/kisspliceGraphAndSequenceTest.py" "${BUILD_BINDIR}")
set_tests_properties(KisspliceGraphAndSequenceTest PROPERTIES PASS_REGULAR_EXPRESSION "test SUCCESSFUL")

# ============================================================================
# Tests on: //data/graph_HBM75brain_100000_HBM75liver_100000_k25 
# ============================================================================
add_test(KisspliceGraphTest "${CMAKE_CURRENT_SOURCE_DIR}/kisspliceGraphTest.py" "${BUILD_BINDIR}")
set_tests_properties(KisspliceGraphTest PROPERTIES PASS_REGULAR_EXPRESSION "test SUCCESSFUL")
