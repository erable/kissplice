# ============================================================================
# Tests on: /data/HBM75brain_1000000.fasta 
#      +   /data/HBM75liver_1000000.fasta
# ============================================================================
add_test(ksBubbleEnumerationTest "${CMAKE_CURRENT_SOURCE_DIR}/ksBubbleEnumerationTest.py" "${BUILD_INTERNAL_BINDIR}")
set_tests_properties(ksBubbleEnumerationTest PROPERTIES PASS_REGULAR_EXPRESSION "test SUCCESSFUL")

# ============================================================================
# Tests on: /data/HBM75brain_1000000.fasta 
#      +   /data/HBM75liver_1000000.fasta
# ============================================================================
add_test(ksRunModulesTest "${CMAKE_CURRENT_SOURCE_DIR}/ksRunModulesTest.py" "${BUILD_INTERNAL_BINDIR}")
set_tests_properties(ksRunModulesTest PROPERTIES PASS_REGULAR_EXPRESSION "test SUCCESSFUL")

# ============================================================================
# Tests on: /data/HBM75brain_1000000.fasta 
#      +   /data/HBM75liver_1000000.fasta
# ============================================================================
add_test(ksErrorRemovalTest "${CMAKE_CURRENT_SOURCE_DIR}/ksErrorRemovalTest.py" "${BUILD_INTERNAL_BINDIR}")
set_tests_properties(ksErrorRemovalTest PROPERTIES PASS_REGULAR_EXPRESSION "test SUCCESSFUL")