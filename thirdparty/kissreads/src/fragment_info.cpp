#include "fragment_info.h"
#include <sstream>
#include <iostream>
#include "Utils.h"

using namespace std;

//set a fragment to be uncoherent in all read sets
void itIsUncoherent(p_fragment_info the_starter, int nbOfReadFiles) {
  for (int i=0;i<nbOfReadFiles;i++)
    the_starter->read_coherent[i]=0;
}

string getNiceDescriptionOfTheConsensusStrand(p_fragment_info the_starter, int readset) {
  stringstream ss;
  ss << the_starter->consensusStrandOfMappedReads[readset] << " (" << the_starter->nbOfReadsMappingInFW[readset] << " reads on +, " <<
      the_starter->nbOfReadsMappingInRC[readset] << " reads on -)";
  return ss.str();
}

void reverseEverything(p_fragment_info path) {
  //reverse the bubbles' sequence
  reverse_complement(path->w);

  //reverse the extensions
  reverse_complement(path->left_extension);
  reverse_complement(path->right_extension);
  swap(path->left_extension, path->right_extension);

  //must reverse everything related to the FW and RC also
  swap(path->nb_reads_fully_in_S_in_FW, path->nb_reads_fully_in_S_in_RC);
  swap(path->nb_reads_overlapping_AS_in_FW, path->nb_reads_overlapping_AS_in_RC);
  swap(path->nb_reads_overlapping_SB_in_FW, path->nb_reads_overlapping_SB_in_RC);
  swap(path->nb_reads_overlapping_both_AS_and_SB_in_FW, path->nb_reads_overlapping_both_AS_and_SB_in_RC);
  swap(path->nbOfReadsMappingInFW, path->nbOfReadsMappingInRC);

  //invert
  invertStrands(path->consensusStrandOfMappedReads, number_of_read_sets);

  //TODO: probably also had to invert tested_pwis_with_current_read, read_coherent_positions, sum_quality_per_position since they refer to positions inside the bubble
  //but since as we don't use this afterwards (in KisSplice case), I am ignoring this...
  //But this is potentially a source of bugs
  //But I think we should refactor all KissReads...

}