#ifndef UTILS_H
#define UTILS_H

char complement(char n);
void reverse_complement(char *seq);
char invertStrand(char strand);
void invertStrands(char * consensusStrandOfMappedReads, int size);

#endif