#!/usr/bin/perl -w


if(@ARGV !=3){
print "format:dot2tab.pl file1.txt file1_edges.txt file1_nodes_attributes.txt \n";
print "Please specify the names of the input and output files\n"; 
exit 1;
}

$infile=shift; 
$outfile1=shift;
$outfile2=shift;

open(IN, "$infile") || die "Cannot open: $!\n";

open(OUT, ">$outfile1") || die "Cannot open: $!\n";


open(OUT2, ">$outfile2") || die "Cannot open the file: $!\n";



while($line=<IN>){
chomp($line);

#3 -> 159919 [label="RF"];
if(($line=~m/^(\d+) -> (\d+) \[label=\"(\w+)\"\]/)){
#print $line;
print OUT "$1\t$2\t$3\n";
}


#191605 [label="AATCATTTCTAATATAGGTTTATTCCATACA / TGTATGGAATAAACCTATATTAGAAATGATT 12:0"];
if($line=~m/^(\d+) \[label=\"(\w+) \/ (\w+) (\d+):(\d+)\"\]/){
print OUT2 "$1\t$2\t$3\t$4\t$5\n";
}

}
