Release procedure for gitlab

# Changelog
List the changes since the last release in the "ChangeLog" file, with the appropriate syntax (http://en.wikipedia.org/wiki/Changelog).
A good practice is to list changes to an "UNRELEASED" entry as they are made, and finalize this entry with the version number during the Release.

# Documentation
If the command line options for `kissplice` changed, consider updating :
- the manpage at man/kissplice.in.1
- the latex user guide at doc/user_guide.in.tex

# Version number
Change the release numbers `project(VERSION ...)` in the file "CMakeLists.txt".
Use the[semver](https://semver.org/lang/fr/) convention.

# Check content
Ensure everything is commited and pushed to gitlab : code, changelog file, documentation.

Check that the build is working :
- either test locally (see `readme.cmake.md`)
- or check that continuous integration tests are ok : https://gitlab.inria.fr/erable/kissplice/-/pipelines

# Create tag
Create a git tag marking the release commit, with the version as name (see previous releases).
The tag must have no message (*lightweight tag* in git terminology).
Create it via either :
- https://gitlab.inria.fr/erable/kissplice/-/tags, "new tag", with no message
- or via git and push : https://git-scm.com/book/en/v2/Git-Basics-Tagging

The reason for no message is because we use the strategy described in https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html#create-a-release-when-a-git-tag-is-created to generate the convenience binary tar.gz.
This strategy requires leaving the creation of the release up to the CI pipeline.
If gitlab allows to append binaries to existing releases it would be easier, but this is not the case for now.
Do not use https://docs.gitlab.com/ee/user/project/releases/#create-a-release, because the CI will fail if the release is already created.

# Finalize the newly created release
Adding the new tag should trigger a new CI run in https://gitlab.inria.fr/erable/kissplice/-/pipelines.
When it completes it should create a new release for the tag version in https://gitlab.inria.fr/erable/kissplice/-/releases, with the convenience binary release.

The new release can then be edited to add release information, like the new changes from the changelog.

The release page automatically creates source tar.gz archives.
Some "developper only" files are excluded from these archives, see `.gitattributes` `export-ignore`.
These archives should work as long as no submodule is added.

# Debian
Kissplice is packaged by the Debian Med team. They track gitlab releases so they should notice them automatically.
Packaging gitlab : https://salsa.debian.org/med-team/kissplice/.
Useful documentation : https://med-team.pages.debian.net/policy/.
Uses external bcalm

# Conda
Packaged by bioconda at https://github.com/bioconda/bioconda-recipes/tree/master/recipes/kissplice.
Uses external bcalm.

# Homebrew (MacOS)
Packaged by brewsci/bio at https://github.com/brewsci/homebrew-bio/blob/develop/Formula/kissplice.rb.
Uses internal patch of bcalm (https://github.com/fgindraud/bcalm) with patches to build on MacOS.
FIXME use bcalm formula as dependency when bcalm merges the PR upstream.